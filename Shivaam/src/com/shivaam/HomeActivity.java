package com.shivaam;



import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.widget.Toast;



public class HomeActivity extends BaseActivity {
	
	
	public FrameLayout frameLayout;
	public SeekBar seek_bar ; 
	public ImageButton play_button, stop_button, pause_button ;  
	public ListView songs_listview;
	public TextView seekbar_progress, seekbar_final;
	Handler seekHandler = new Handler();
	MediaPlayer player = new MediaPlayer();
	
    @SuppressLint("InflateParams") 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        super.onCreateDrawer(savedInstanceState);
//       
        	
                
        FrameLayout frameLayout = (FrameLayout)findViewById(R.id.frame_layout);
        // inflate the custom activity layout
        LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View activityView = layoutInflater.inflate(R.layout.activity_home, null,false);
        // add the custom layout of this activity to frame layout.
        frameLayout.addView(activityView);
        
            
        // attaching the header to the listview  
         View headerView = getLayoutInflater().inflate(R.layout.header, null, false);
        songs_listview = (ListView) findViewById(R.id.songs_listview);
            songs_listview.addHeaderView(headerView);

        //Coding for SeekBar and ListView..
        seekbar_progress = (TextView) findViewById(R.id.seekbar_progress);
        seekbar_final = (TextView) findViewById(R.id.seekbar_final);
        seek_bar = (SeekBar)findViewById(R.id.seek_bar);
        seek_bar.setEnabled(false);
        (seekbar_progress).setText("0.00");
		seek_bar.setMax(0);

        seekUpdate();
 
    	//Initializing ListView
       	
        String[] items = new String[] { "Pittha Pirai", "Thodudaiya", "Om Namashivaya", "Poovaar", "Lingastagam" };  
        
       
        songs_listview.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_activated_1,
                items){
		        	@Override
		        	public boolean areAllItemsEnabled(){
		        		return true;
		        	}
		        }); 
        
        
        final Activity mContext = this;

        songs_listview.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
            	 
    				switch(pos) {
    				
    				case 1: { 
    					if(player.isPlaying()){ player.stop(); player.reset(); player.release();}
    					player = MediaPlayer.create(mContext, R.raw.pitthapirai);
    					
    					//(seekbar_final).setText(seek_bar.getMax());

    					play(arg0);
    					break;
    				}
    				case 2: { 
    					if(player.isPlaying()){ player.stop(); player.reset(); player.release();}

    					player = MediaPlayer.create(mContext, R.raw.thodudaiya);
    					play(arg0);
    					break;
    				}
    			    case 3: { 
    					if(player.isPlaying()){ player.stop(); player.reset(); player.release();}

    			    	player = MediaPlayer.create(mContext, R.raw.omnama);
    			    	play(arg0);
    					break;
    				}
    			    case 4: { 
    					if(player.isPlaying()){ player.stop(); player.reset(); player.release();}

    			    	player = MediaPlayer.create(mContext, R.raw.poovaar);
    			    	play(arg0);
    					break;
    				}
    			    case 5: { 
    					if(player.isPlaying()){ player.stop(); player.reset(); player.release();}

    			    	player = MediaPlayer.create(mContext, R.raw.lingashtakam);
    			    	play(arg0);
    					break;
    				}
   			    
    			    default: 
    					if(player.isPlaying()){ player.stop(); player.reset(); player.release();}

    			    	player = MediaPlayer.create(mContext, R.raw.pitthapirai);
    			    	play(arg0);
    					break;
    				}		
            }
        });
        

    }
	
	Runnable run = new Runnable(){
		@Override
		public void run() {
			// TODO Auto-generated method stub
			seekUpdate();
		}
	};
	
	
	
	
	
	public void seekUpdate(){
		if(player.isPlaying()){
		(seekbar_progress).setText(""+milliSecondsToTimer(player.getCurrentPosition()));
		}
		seek_bar.setProgress(player.getCurrentPosition());
		seekHandler.postDelayed(run, 1000);
	}
	
	public void play(View view){
		player.start();
		
		if(player.isPlaying()){
			(seekbar_final).setText(""+milliSecondsToTimer(player.getDuration()));
			seek_bar.setMax(player.getDuration());

		}else {
			(seekbar_final).setText("");
			seek_bar.setMax(0);

		}
		
	}
	 
	public void pause(View view){
		player.pause();
	}
	
	public void stop(View view){
		player.stop();
		seek_bar.setMax(0);
        (seekbar_progress).setText("0.00");
		player.reset();

	}
	
	
	public String milliSecondsToTimer(long milliseconds){
        String finalTimerString = "";
        String secondsString = "";
 
        // Convert total duration into time
           int hours = (int)( milliseconds / (1000*60*60));
           int minutes = (int)(milliseconds % (1000*60*60)) / (1000*60);
           int seconds = (int) ((milliseconds % (1000*60*60)) % (1000*60) / 1000);
           // Add hours if there
           if(hours > 0){
               finalTimerString = hours + ":";
           }
 
           // Prepending 0 to seconds if it is one digit
           if(seconds < 10){
               secondsString = "0" + seconds;
           }else{
               secondsString = "" + seconds;}
 
           finalTimerString = finalTimerString + minutes + ":" + secondsString;
 
        // return timer string
        return finalTimerString;
    }
	
	
	public int progressToTimer(int progress, int totalDuration) {
        int currentDuration = 0;
        totalDuration = (int) (totalDuration / 1000);
        currentDuration = (int) ((((double)progress) / 100) * totalDuration);
 
        // return current duration in milliseconds
        return currentDuration * 1000;
    }

	
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
       // int id = item.getItemId();
       /* if (id == R.id.action_settings) {
            return true;
        }*/
      
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    protected void onDestroy() {
   	
    	player.stop();
    	player.reset();
    	super.onDestroy();
    
    }
}