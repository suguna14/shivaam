package com.shivaam;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.shivaam.R;


public class BaseActivity extends ActionBarActivity {
	
	public DrawerLayout drawerLayout;
    public ListView drawerList;
    private ActionBarDrawerToggle drawerToggle;
    
    private Toolbar toolbar;
	
//    protected void onCreateDrawer(Bundle savedInstanceState) {
    
    
     protected void onCreate(Bundle savedInstanceState) {
    	  
    	  super.onCreate(savedInstanceState);
    	  setContentView(R.layout.drawer_layout);
    	  //toolbar initialization
    	  toolbar = (Toolbar) findViewById(R.id.toolbar_id);
          setSupportActionBar(toolbar);
          
//          getSupportActionBar().setDisplayShowHomeEnabled(true);
//      	  getSupportActionBar().setDisplayHomeAsUpEnabled(true);
  	
    	// R.id.drawer_layout should be in every activity with exactly the same id.
    	drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
    	
    	drawerToggle = new ActionBarDrawerToggle((Activity) this, drawerLayout, R.string.drawer_open, R.string.drawer_close) 
    	{
	        public void onDrawerClosed(View drawerView) 
	        {
	        	super.onDrawerClosed(drawerView);
	        	invalidateOptionsMenu();
	        	//drawerToggle.syncState();
	        }
	
	        public void onDrawerOpened(View drawerView) 
	        {
	        	super.onDrawerOpened(drawerView);
	        	invalidateOptionsMenu();
	        	//drawerToggle.syncState();
	        }
	        
	        public void onDrawerSlide(View drawerView, float slideOffset) 
	        {
	        	toolbar.setAlpha(1- slideOffset/2);
	        	
	        }
	    };
	    drawerLayout.setDrawerListener(drawerToggle);
	
	    
	    //Populating ListView
	    drawerList = (ListView) findViewById(R.id.left_drawer);
	    String[] items = new String[] { "Songs", "Rudram", "Various forms of Shiva", "His Cosmic Dance", "Festivals" };  
	    drawerList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_activated_1,
	            items)); //Items is an ArrayList or array with the items you want to put in the Navigation Drawer.
	
	    final Activity mContext = this;
	    drawerList.setOnItemClickListener(new OnItemClickListener() {
	        @Override
	        public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
	        	 
					switch(pos) {
					
					case 0: { 
						Intent newIntent = new Intent(mContext, HomeActivity.class);
						newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(newIntent);
						break;
					}
					case 1: { 
						Intent newIntent = new Intent(mContext, AboutshivaActivity.class);
						
						newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(newIntent);
						break;
					}
				    case 2: { 
						Intent newIntent = new Intent(mContext, PhotosActivity.class);
						newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(newIntent);
						break;
					}
				    case 3: { 
						Intent newIntent = new Intent(mContext, CosmicdanceActivity.class);
						newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(newIntent);
						break;
					}
					case 4: { 
						Intent newIntent = new Intent(mContext, FestivalsActivity.class);
						newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(newIntent);
						break;
						
					}
					default: 
						Intent newIntent = new Intent(mContext, HomeActivity.class);
						newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(newIntent);
					}		
	        }
	    });
	    
	    drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                drawerToggle.syncState();
            }
        });
    }
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
	
	    if (drawerToggle.onOptionsItemSelected(item)) {
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	
	}
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    }
	
	
}
