package com.shivaam;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;


public class SplashActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
     
        //Create a thread for the splash 
        
        Thread thread = new Thread(){
        	public void run(){
        		try {
					sleep(2*1000);
					startActivity(new Intent(getBaseContext(), HomeActivity.class));
	        		finish();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
        	}
        };
        
        thread.start();
        
    }
    
    @Override
    protected void onDestroy() {
    super.onDestroy();
  
    }

}
