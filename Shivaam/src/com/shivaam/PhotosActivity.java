package com.shivaam;

import android.support.v4.widget.DrawerLayout;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ViewFlipper;


public class PhotosActivity extends BaseActivity {

	
	DrawerLayout mDrawerLayout = null;  
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        
        FrameLayout frameLayout = (FrameLayout)findViewById(R.id.frame_layout);
        // inflate the custom activity layout
        LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View activityView = layoutInflater.inflate(R.layout.activity_photos, null,false);
        // add the custom layout of this activity to frame layout.
        frameLayout.addView(activityView);
        // now you can do all your other stuffs
  	
    	viewFlipperToDisplayImages();
   	
    }
    	
    public void viewFlipperToDisplayImages(){

    	Animation slide_in_left, slide_out_right;
    	
    	Button btnPrevious = (Button) findViewById(R.id.previous);
    	Button btnNext = (Button) findViewById(R.id.next);
    	Button slideshow = (Button) findViewById(R.id.slideshow);
    	Button btnStop = (Button) findViewById(R.id.stop);

    	final ViewFlipper viewFlipper = (ViewFlipper) findViewById(R.id.viewflipper);

    	slide_in_left = AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left);
    	slide_out_right = AnimationUtils.loadAnimation(this, android.R.anim.slide_out_right);

    	viewFlipper.setInAnimation(slide_in_left);
    	viewFlipper.setOutAnimation(slide_out_right);

    	btnPrevious.setOnClickListener(new OnClickListener() {
	    	   @Override
	    	   public void onClick(View arg0) {
	    	    viewFlipper.showPrevious();
	    	   }
    	});

    	btnNext.setOnClickListener(new OnClickListener() {
	    	   @Override
	    	   public void onClick(View arg0) {
	    	    viewFlipper.showNext();
	    	   }
    	});
    	
    	
    	slideshow.setOnClickListener(new OnClickListener(){
    			@Override
	    	    public void onClick(View arg0) {
    			  viewFlipper.setAutoStart(true);
    			  viewFlipper.setFlipInterval(2000);
    			  viewFlipper.startFlipping();
			    }
	    		
	    	});
	    	
	    btnStop.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					viewFlipper.stopFlipping();
					viewFlipper.setInAnimation(null);
			    	viewFlipper.setOutAnimation(null);
				}
	  		
	    	});
    	}



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
       // int id = item.getItemId();
        /*if (id == R.id.action_settings) {
            return true;
        }*/
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    protected void onDestroy() {
    super.onDestroy();

    
    }
    
}
